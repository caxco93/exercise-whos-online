const whosOnline = require('./index');

test("Example test one of each", () => {
    const input = [{
        username: 'David',
        status: 'online',
        lastActivity: 10
    }, {
        username: 'Lucy',
        status: 'offline',
        lastActivity: 22
    }, {
        username: 'Bob',
        status: 'online',
        lastActivity: 104
    }];

    expect(whosOnline(input)).toStrictEqual({
        online: ['David'],
        offline: ['Lucy'],
        away: ['Bob']
    });
});

test("Example test no one online", () => {
    const input = [{
        username: 'Lucy',
        status: 'offline',
        lastActivity: 22
    }, {
        username: 'Bob',
        status: 'online',
        lastActivity: 104
    }];

    expect(whosOnline(input)).toStrictEqual({
        offline: ['Lucy'],
        away: ['Bob']
    });
});
